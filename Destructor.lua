return class "Destructor" (class.Annotation)
{
	addDestructorCall = function(target, func)
		local proxy = newproxy()
		debug.setmetatable(proxy, {__gc = function()
			return func(target)
		end})
	end,

	apply = function(self, f, name, class)
		local oldnew = class.__new__
		function class.__new__(cls)
			local instance = oldnew(cls)
			self.addDestructorCall(instance, f)
			return instance
		end
	end,
}
