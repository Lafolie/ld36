local requireall = require "lib.requireall"
require "middleman"

-- GLOBALS, woo
class = require "lib.slither"
Gamestate = require "lib.hump.gamestate"
state = requireall("state")

function love.load()
	require("lib.repler").load()

	Gamestate.registerEvents()

	if not arg[2] then arg[2] = "splash" end
	Gamestate.switch(state[arg[2]])
end

function love.keypressed(key)
	if key == "escape" then
		return love.event.quit()
	end
end
