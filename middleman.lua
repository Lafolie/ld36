local signal = require "lib.hump.signal"

--- Chat ---
signal.register("recv-Chat", function(msg)
	signal.emit("Chat", false, msg)
end)

signal.register("Chat", function(isLocal, msg)
	if isLocal then
		signal.emit("send-Chat", msg)
	end
end)

--- Cosmetic ---
signal.register("recv-Cosmetic", function(action)
	if action.type == "HandHover" then
		signal.emit(action.type, false, action.hovering)
	end
end)

signal.register("HandHover", function(isLocal, hovering)
	if not isLocal then return end

	local action = {
		type = "HandHover",
		hovering = hovering,
	}
	signal.emit("send-Cosmetic", action)
end)

--- Command ---
signal.register("recv-Command", function(action)
	if action.type == "PlayCard" then
		signal.emit(action.type, false, action.id, action.target)
	elseif action.type == "Draw" then
		signal.emit(action.type, false, nil)
	elseif action.type == "Discard" then
		signal.emit(action.type, false, nil)
	elseif action.type == "EndTurn" then
		signal.emit(action.type, false)
	elseif action.type == "Ability" then
		signal.emit(action.type, false, action.ability, action.golem, action.target)
	end
end)

signal.register("PlayCard", function(isLocal, id, target)
	if not isLocal then return end

	local action = {
		type = "PlayCard",
		id = id,
		target = (target+3)%8+1,
	}
	signal.emit("send-Command", action)
end)

signal.register("Draw", function(isLocal, card)
	if not isLocal then return end

	local action = {
		type = "Draw",
	}
	signal.emit("send-Command", action)
end)

signal.register("Discard", function(isLocal, card)
	if not isLocal then return end

	local action = {
		type = "Discard",
	}
	signal.emit("send-Command", action)
end)

signal.register("EndTurn", function(isLocal)
	if not isLocal then return end

	local action = {
		type = "EndTurn",
	}
	signal.emit("send-Command", action)
end)

signal.register("Ability", function(isLocal, ability, golem, target)
	if not isLocal then return end

	local action = {
		type = "Ability",
		ability = ability,
		golem = (golem+3)%8+1,
		target = target and (target+3)%8+1 or nil,
	}
	signal.emit("send-Command", action)
end)
