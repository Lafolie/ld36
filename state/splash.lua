local menu = require "state.menu"

local splash = {}

function splash:init()
end

function splash:enter()
	self.timer = 0
	self.runTime = 2
end

function splash:update(dt)
	self.timer = self.timer + dt
	if self.timer >= self.runTime then
		return Gamestate.switch(menu)
	end
end

function splash:keypressed(key)
	return Gamestate.switch(menu)
end

function splash:draw()
	love.graphics.setColor(212, 166, 106)
	love.graphics.rectangle('fill', 0, 0, 1280, 720)

	local a = 0
	a = 1-(self.runTime-self.timer)/self.runTime
	a = 255 * (a*a)
	love.graphics.setColor(69, 138, 121, a)
	love.graphics.print("GAME", 450, 200, 0, 10, 10)
end

return splash
