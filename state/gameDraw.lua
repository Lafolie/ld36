local signal = require "lib.hump.signal"
local Hand = require "Hand"

local gameDraw = {}

function gameDraw:enter(parent)
	self.parent = parent
	self.numCards = 7
	self.hand = Hand()
	self:drawCards()
end

function gameDraw:drawCards()
	local num = math.min(self.numCards, 6)
	self.numCards = self.numCards - 1

	while #self.hand.cards > 0 do
		local card = self.hand:removeByPosition(1)
		self.parent.players.own.deck:addCard(card)
	end

	self.parent.players.own.deck:shuffle()

	for i = 1, num do
		local card = self.parent.players.own.deck:drawCard()
		self.hand:add(card)
	end
end

-- FIXME: proper buttons
function gameDraw:keypressed(key)
	if key == "m" then -- mulligan
		self:drawCards()
	elseif key == "a" then
		Gamestate.pop()
		for i, v in ipairs(self.hand.cards) do
			signal.emit("Draw", true, v)
		end
	end
end

function gameDraw:update(dt)
	self.parent:alwaysUpdate(dt)

	local x, y = love.mouse.getPosition()
	self.hand:hover(x, y, false)
end

function gameDraw:draw()
	self.parent:draw()
	self.hand:draw(false)
end

return gameDraw
