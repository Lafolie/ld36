local deckEditor = {}
local Card = require "Card"
local CardStack = require "CardStack"

function deckEditor:enter()
	love.graphics.setBackgroundColor(212, 166, 106)
	self.deck = CardStack{}
	self.deck:add(Card{art = "gfx/front.png"})
	self.deck:add(Card{art = "gfx/front.png"})
	self.deck:add(Card{art = "gfx/front.png"})
	self.deck:add(Card{art = "gfx/frontAlt.png"})
	self.deck:add(Card{art = "gfx/front.png"})
	self.deck:add(Card{art = "gfx/front.png"})
	self.deck:add(Card{art = "gfx/frontAlt.png"})
	
	
	self.cardPool = {
		Card{art = "gfx/front.png"},
		Card{art = "gfx/front.png"},
		Card{art = "gfx/front.png"},
		Card{art = "gfx/front.png"},
		Card{art = "gfx/front.png"},
		Card{art = "gfx/front.png"},
		Card{art = "gfx/front.png"},
		Card{art = "gfx/front.png"},
		Card{art = "gfx/front.png"},
		Card{art = "gfx/front.png"},
		
		Card{art = "gfx/frontAlt.png"},
		Card{art = "gfx/frontAlt.png"},
		Card{art = "gfx/frontAlt.png"},
		Card{art = "gfx/frontAlt.png"},
		Card{art = "gfx/frontAlt.png"},
		Card{art = "gfx/frontAlt.png"},
		Card{art = "gfx/frontAlt.png"},
		Card{art = "gfx/frontAlt.png"},
		
		Card{art = "gfx/front.png"},
		Card{art = "gfx/front.png"}
		}
	self.page = 0
	self.rowSize = 6
	self.maxPage = math.ceil(#self.cardPool / self.rowSize) - 2
	
	self.selectedX = 1
	self.selectedY = 1
	
	self.deckSelected = nil
end

function deckEditor:keypressed(key)
	if key == "left" then
		if self.deckSelected then
			self.deckSelected = nil
		elseif self.selectedX > 1 then
			self.selectedX = self.selectedX - 1
		end
	elseif key == "right" then
		if self.selectedX < self.rowSize and (#self.cardPool > (self.page+self.selectedY-1)*self.rowSize + self.selectedX) then
			self.selectedX = self.selectedX + 1
		elseif not self.deckSelected and #self.deck.cards > 0 then
			self.deckSelected = 1
		end
	elseif key == "up" then
		if self.deckSelected then
			if self.deckSelected > 1 then
				self.deckSelected = self.deckSelected - 1
			end
		elseif self.selectedY ~= 1 then
			self.selectedY = self.selectedY -1
		elseif self.page > 0 then
			self.page = self.page - 1
		end
	elseif key == "down" then
		if self.deckSelected then
			if self.deckSelected < #self.deck.cards then
				self.deckSelected = self.deckSelected + 1
			end
		elseif self.selectedY ~= 2 then
			self.selectedY = self.selectedY + 1
		elseif self.page < self.maxPage then
			self.page = self.page + 1
		end
		if #self.cardPool < (self.page+self.selectedY-1)*self.rowSize + self.selectedX then
			self.selectedX = #self.cardPool - (self.page+self.selectedY-1)*self.rowSize
		end
	elseif key == "delete" then
		if self.deckSelected then
			self.deck:remove(self.deck.cards[self.deckSelected])
		end
	elseif key == "return" then
		if not self.deckSelected then
			local clone = self.cardPool[(self.page+self.selectedY-1)*self.rowSize + self.selectedX]:clone()
			self.deck:add(clone)
		end
	end
end

function deckEditor:update(dt)
end

function deckEditor:draw()
	self.deck:drawList(1150, 100, self.deckSelected)
	--self.deck:drawPile(300, 300)
	for i, v in ipairs(self.cardPool) do
		if i > self.page * self.rowSize and i <= (self.page + 2) * self.rowSize then
			if(not self.deckSelected and (i-1)%self.rowSize +1 == self.selectedX) and ((self.selectedY == 1 and i <= (self.page + 1) * self.rowSize) or (self.selectedY == 2 and i > (self.page+1) * self.rowSize)) then
				v.height = 0.1
			else
				v.height = 0
			end
			v:draw((((i-1)%self.rowSize)*150)+100, i> (self.page+1) * self.rowSize and 500 or 250)	
		end
	end
	
end

return deckEditor
