local gameTargetSelect = {}

function gameTargetSelect:enter(parent, card, extra)
	self.parent = parent
	self.card = card
	self.extra = extra
end

function gameTargetSelect:update(dt)
	self.parent:alwaysUpdate(dt)
end

function gameTargetSelect:mousepressed(x, y, button)
	if button ~= 1 then return end

	for i, golem, opp in self.parent:allGolems() do
		golem:hover(x, y, opp)
		if golem.hovering then
			Gamestate.pop(self.card, i, self.extra)
			break
		end
	end
end

function gameTargetSelect:wheelmoved(dx, dy)
	return self.parent:wheelmoved(dx, dy)
end

function gameTargetSelect:draw()
	self.parent:draw()

	self.card.height = 0.4
	self.card:draw(720, 600)
end

return gameTargetSelect
