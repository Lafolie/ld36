local signal = require "lib.hump.signal"
local Network = require "Network"
local CombatCardStack = require "CombatCardStack"
local Hand = require "Hand"
local Database = require "Database"
local Deck = require "Deck"
local Card = require "Card"
local Golem = require "cards.Golem"
local Relic = require "cards.Relic"
local Crystal = require "cards.Crystal"

local gameDraw = require "state.gameDraw"
local gameTargetSelect = require "state.gameTargetSelect"
local gameActivateGolem = require "state.gameActivateGolem"

local game = {}

local function Player(cards, opponent)
	local player = {
		hand = Hand(),
		deck = Deck(cards),
		discard = Deck(),
		golems = {},
	}

	local deckGolems = player.deck:removeGolems()

	for i = 1, 4 do
		player.golems[i] = CombatCardStack(224+(i-1)*208, opponent and 240 or 410)
		player.golems[i].height = -0.2
		player.golems[i]:add(deckGolems[i])

		if opponent then
			player.golems[i]:flipBack()
		end
	end

	return player
end

function game:enter(previous, serverIp)
	love.graphics.setBackgroundColor(80, 22, 50)

	self.network = Network()

	if not serverIp then
		self.network:startServer()
		self.myTurn = true
	else
		self.network:connect(serverIp)
		self.myTurn = false
	end

	self.golemDb = Database:getTable("golems")
	self.relicDb = Database:getTable("relics")
	self.crystalDb = Database:getTable("crystals")

	local cards = game:enterFIXME()

	self.players = {
		own = Player(cards.own, false),
		opponent = Player(cards.opponent, true),
	}

	signal.register("Draw", self.drawCard)
	signal.register("HandHover", self.handHover)
	signal.register("PlayCard", self.playCard)
	signal.register("Discard", self.discardCard)
	signal.register("EndTurn", self.endTurn)
	signal.register("Ability", self.abilityUsed)

	Gamestate.push(gameDraw)
end

function game:resume(from, ...)
	if from == gameDraw then
		for i = 1, 4 do
			self.players.opponent.golems[i]:flipFront()
		end
	elseif from == gameTargetSelect then
		local card, target, extra = ...
		if extra and extra.ability then
			signal.emit("Ability", true, extra.ability, extra.golem, target)
			signal.emit("EndTurn", true)
		else
			signal.emit("PlayCard", true, card.id, target)
		end
	elseif from == gameActivateGolem then
		local golem, ability = ...
		if ability then
			if ability:needsTarget() then
				local golemStack = self:golemIdToGolem(golem)
				Gamestate.push(gameTargetSelect, golemStack.golem, {ability = ability:serialize(), golem = golem})
			else
				signal.emit("Ability", true, ability:serialize(), golem)
				signal.emit("EndTurn", true)
			end
		end
	end
end

function game:leave()
	self.network:close()
	self.network = nil

	signal.remove("Ability", self.abilityUsed)
	signal.remove("EndTurn", self.endTurn)
	signal.remove("Discard", self.discardCard)
	signal.remove("PlayCard", self.playCard)
	signal.remove("HandHover", self.handHover)
	signal.remove("Draw", self.drawCard)
end

function game.drawCard(isLocal, card)
	local self = game -- :(

	if isLocal then
		self.players.own.hand:add(card)
	else
		self.players.opponent.hand:add(Card{art = "gfx/front.png"})
		self.players.opponent.deck:drawCard()
	end
end

function game.handHover(isLocal, hovering)
	local self = game -- :(
	if not isLocal then
		self.players.opponent.hand.hovering = hovering
	end
end

function game.playCard(isLocal, id, target)
	local self = game -- :(
	local card = self:findCard(id)
	local stack = self:golemIdToGolem(target)
	stack:add(card)

	if not isLocal then
		self.players.opponent.hand:removeByPosition(1)
	end
end

function game.discardCard(isLocal, card)
	local self = game -- :(
	if isLocal then
		self.players.own.discard:addCard(card)
	else
		local card = self.players.opponent.hand:removeByPosition(1)
		self.players.opponent.discard:addCard(card)
	end
end

function game.endTurn(isLocal)
	local self = game -- :(
	if isLocal == self.myTurn then
		self.myTurn = not self.myTurn

		if self.myTurn then
			local card = self.players.own.deck:drawCard()
			signal.emit("Draw", true, card)
		end
	end
end

function game.abilityUsed(isLocal, ability, golem, target)
	local self = game -- :(
	if target ~= nil then
		target = self:golemIdToGolem(target)
	end

	golem = self:golemIdToGolem(golem)
	ability = Card.parseAbilities(ability)[1]
	ability:activate(golem, target)
end

function game:golemIdToGolem(id)
	if id > 4 then
		return self.players.opponent.golems[id-4]
	else
		return self.players.own.golems[id]
	end
end

function game:enterFIXME() -- FIXME
	for i, v in pairs(self.golemDb) do
		v.art = v.art or love.filesystem.isFile("gfx/" .. v.id .. ".png") and "gfx/" .. v.id .. ".png"
		v.art = v.art or love.math.random() < 0.5 and "gfx/front.png" or "gfx/frontAlt.png"
	end
	for i, v in pairs(self.relicDb) do
		v.art = v.art or love.filesystem.isFile("gfx/" .. v.id .. ".png") and "gfx/" .. v.id .. ".png"
		v.art = v.art or love.math.random() < 0.5 and "gfx/front.png" or "gfx/frontAlt.png"
	end
	for i, v in pairs(self.crystalDb) do
		v.art = v.art or love.filesystem.isFile("gfx/" .. v.id .. ".png") and "gfx/" .. v.id .. ".png"
		v.art = v.art or love.math.random() < 0.5 and "gfx/front.png" or "gfx/frontAlt.png"
	end

	local cards = {own = {}, opponent = {}}
	for i, v in ipairs{
			"yggdrasil",
			"yggdrasil",
			"yggdrasil",
			"yggdrasil",
			"flameCannon",
			"flameCannon",
			"flameCannon",
			"flameCannon",
			"obsidianHull"} do
		cards.own[i] = self:findCard(v)
		cards.opponent[i] = self:findCard(v)
	end
	for i = #cards.own, 64 do
		cards.own[i] = self:findCard("flameCannon")
		cards.opponent[i] = self:findCard("flameCannon")
	end
	return cards
end

function game:findCard(id)
	if self.golemDb[id] then
		return Golem(self.golemDb[id])
	elseif self.relicDb[id] then
		return Relic(self.relicDb[id])
	elseif self.crystalDb[id] then
		return Crystal(self.crystalDb[id])
	else
		error("Unknown card: " .. id)
	end
end

local function nextGolem(self, i)
	i = (i or 0) + 1
	if i > 8 then
		return nil
	elseif i > 4 then
		return i, self.players.opponent.golems[i-4], true
	else
		return i, self.players.own.golems[i], false
	end
end

function game:allGolems()
	return nextGolem, self, nil
end

function game:mousepressed(x, y, button)
	if button == 1 and self.myTurn then
		local card = self.players.own.hand:pick(x, y, false)
		if card then
			Gamestate.push(gameTargetSelect, card)
			return
		end

		for i, golem in ipairs(self.players.own.golems) do
			golem:hover(x, y, false)
			if golem.hovering then
				Gamestate.push(gameActivateGolem, i)
				return
			end
		end
	elseif button == 2 and self.myTurn then
		local card = self.players.own.hand:pick(x, y, false)
		if card then
			signal.emit("Discard", true, card)
			return
		end
	end
end

function game:wheelmoved(dx, dy)
	local x, y = love.mouse.getPosition()
	if dy ~= 0 then
		for _, golem, opp in self:allGolems() do
			golem:hover(x, y, opp)
			if golem.hovering then
				-- TODO: Zoom
				return
			end
		end
	end
end

function game:keypressed(key)
	if key == "return" then -- FIXME
		signal.emit("EndTurn", true)
	end
end

function game:alwaysUpdate(dt)
	self.network:update()
end

function game:update(dt)
	self:alwaysUpdate(dt)

	local x, y = love.mouse.getPosition()
	for i, v, opp in self:allGolems() do
		v:hover(x, y, opp)
	end

	self.players.own.hand:hover(x, y, false)
end

function game:draw()
	love.graphics.setColor(255, 255, 255)

	-- Background
	-- TODO

	for i, v, opp in self:allGolems() do
		v:draw(opp)
	end

	-- Battlefield
	-- TODO

	-- Player 1
	self.players.own.deck:draw(1184, 620, false)
	self.players.own.discard:draw(96, 620, false)
	self.players.own.hand:draw(false)

	-- Player 2
	self.players.opponent.deck:draw(96, 100, true)
	self.players.opponent.discard:draw(1184, 100, true)
	self.players.opponent.hand:draw(true)

	-- Chatlog
	-- TODO
end

return game
