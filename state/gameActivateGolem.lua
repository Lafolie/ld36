local gameActivateGolem = {}

function gameActivateGolem:enter(parent, golemId)
	self.parent = parent
	self.golemId = golemId
	self.golem = self.parent.players.own.golems[golemId]
end

function gameActivateGolem:update(dt)
	self.parent:alwaysUpdate(dt)
end

function gameActivateGolem:wheelmoved(dx, dy)
	return self.parent:wheelmoved(dx, dy)
end

function gameActivateGolem:mousepressed(x, y, button)
	local ox, oy = self.golem.x, self.golem.y
	local font = love.graphics.getFont()
	local h = font:getHeight()
	for i, v in ipairs(self.golem.activatableAbilities) do
		local w = font:getWidth(v.__name__)
		if x >= ox and x < ox+w and y >= oy and y < oy+h then
			return Gamestate.pop(self.golemId, v)
		end
		oy = oy + 16
	end
	Gamestate.pop()
end

function gameActivateGolem:draw()
	self.parent:draw()

	local x, y = self.golem.x, self.golem.y
	for i, v in ipairs(self.golem.activatableAbilities) do
		love.graphics.print(v.__name__, x, y)
		y = y + 16
	end
end

return gameActivateGolem
