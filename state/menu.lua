
local test = require "state.test"
local deckEditor = require "state.deckEditor"
local game = require "state.game"

local menu = {}

local options =
{
	{
		text = "Server",
		cb = function()
			return Gamestate.switch(game, nil)
		end
	},

	{
		text = "Client",
		cb = function()
			return Gamestate.switch(game, "localhost")
		end
	},

	{
		text = "Deck Editor",
		cb = function()
			return Gamestate.push(deckEditor)
		end
	},

	{
		text = "Exit",
		cb = function()
			return love.event.quit()
		end
	},
}

function menu:init()
end

function menu:enter()
	self.selection = 1
end

function menu:keypressed(key)
	if key == "return" then
		options[self.selection].cb()
	elseif key == "escape" then
		love.event.quit()
	elseif key == "up" then
		self.selection = (self.selection - 2) % #options + 1
	elseif key == "down" then
		self.selection = self.selection % #options + 1
	end
end

function menu:draw()
	love.graphics.setColor(212, 166, 106)
	love.graphics.rectangle('fill', 0, 0, 1280, 720)
	
	love.graphics.setColor(69, 138, 121, 255)
	love.graphics.printf("GAME",0, 100, 128,"center", 0, 10, 10)
	
	for i, v in ipairs(options) do
		if self.selection == i then
			love.graphics.setColor(84, 66, 42)
			love.graphics.rectangle("fill", 436, 249 + i*90, 405, 80 )
			love.graphics.setColor(219, 200, 175)
			love.graphics.rectangle("fill", 440, 250 + i*90, 400, 75 )
			love.graphics.setColor(69, 138, 121, 255)
		end
		love.graphics.printf(v.text,0, 250 + i*90, 256,"center", 0, 5, 5)
	end
end

return menu
