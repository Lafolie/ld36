local signal = require "lib.hump.signal"

local Card = require "Card"
local Network = require "Network"
local CombatCardStack = require "CombatCardStack"
local Hand = require "Hand"
local Database = require "Database"
local Golem = require "cards.Golem"
local Relic = require "cards.Relic"
local Crystal = require "cards.Crystal"
local Deck = require "Deck"

local test = {}

local chatlog = {}
function chatlog.add(isLocal, text)
	local msg = ("<%s> %s"):format(isLocal and "you" or "opponent", text)
	table.insert(chatlog, msg)

	if #chatlog > 10 then
		table.remove(chatlog, 1)
	end
end

function test:enter()
	love.graphics.setBackgroundColor(80, 22, 50)

	self.golemDb = Database:getTable("golems")
	self.relicDb = Database:getTable("relics")
	self.crystalDb = Database:getTable("crystals")

	-- FIXME
	for i, v in pairs(self.golemDb) do
		v.art = v.art or love.filesystem.isFile("gfx/" .. v.id .. ".png") and "gfx/" .. v.id .. ".png"
		v.art = v.art or love.math.random() < 0.5 and "gfx/front.png" or "gfx/frontAlt.png"
	end
	for i, v in pairs(self.relicDb) do
		v.art = v.art or love.filesystem.isFile("gfx/" .. v.id .. ".png") and "gfx/" .. v.id .. ".png"
		v.art = v.art or love.math.random() < 0.5 and "gfx/front.png" or "gfx/frontAlt.png"
	end
	for i, v in pairs(self.crystalDb) do
		v.art = v.art or love.filesystem.isFile("gfx/" .. v.id .. ".png") and "gfx/" .. v.id .. ".png"
		v.art = v.art or love.math.random() < 0.5 and "gfx/front.png" or "gfx/frontAlt.png"
	end

	self.hands = {
		own = Hand(),
		opponent = Hand(),
	}

	do
		-- FIXME
		local cards = {
			"flameCannon",
			"flameCannon",
			"flameCannon",
			"flameCannon",
			"obsidianHull",
		}
		self.decks = {
			own = Deck{unpack(cards)},
			opponent = Deck{unpack(cards)},
		}

		self.decks.own:shuffle()
	end

	self.discards = {
		own = Deck(),
		opponent = Deck(),
	}

	self.hands.own:add(Card(self.relicDb.flameCannon))
	self.hands.own:add(Card(self.relicDb.obsidianHull))
	self.hands.own:add(Crystal(self.crystalDb.crystal))

	self.hands.opponent:add(Card(self.relicDb.flameCannon))
	self.hands.opponent:add(Card(self.relicDb.flameCannon))
	self.hands.opponent:add(Card(self.relicDb.flameCannon))

	self.hands.opponent:flipBack()

	self.network = Network()
	signal.register("Chat", chatlog.add)
	signal.register("HandHover", test.handHover)
	signal.register("PlayCard", test.playCard)
	signal.register("Draw", test.drawCard)
	signal.register("Discard", test.discardCard)

	self.golems = { own = {}, opponent = {} }
	for i = 1, 4 do
		self.golems.own[i] = CombatCardStack(224+(i-1)*208, 410)
		self.golems.opponent[i] = CombatCardStack(224+(i-1)*208, 240)

		self.golems.own[i].height = -0.2
		self.golems.opponent[i].height = -0.2
	end

	-- FIXME
	for i = 1, 4 do
		self.golems.own[i]:add(Golem(self.golemDb.yggdrasil))
		self.golems.opponent[i]:add(Golem(self.golemDb.yggdrasil))
	end

	self.dummyCard = Card(self.golemDb.colossus)

	-- FIXME
	for i, v in ipairs(arg) do
		if v == "server" then
			self.network:startServer()
		elseif v == "client" then
			self.network:connect("localhost")
		end
	end
end

function test:leave()
	self.network:close()
	self.network = nil
	signal.remove("Chat", chatlog.add)
	signal.remove("HandHover", test.handHover)
	signal.remove("PlayCard", test.playCard)
	signal.remove("Draw", test.drawCard)
	signal.remove("Discard", test.discardCard)
end

function test:findCard(id)
	if self.golemDb[id] then
		return Golem(self.golemDb[id])
	elseif self.relicDb[id] then
		return Relic(self.relicDb[id])
	elseif self.crystalDb[id] then
		return Crystal(self.crystalDb[id])
	else
		error("Unknown card: " .. id)
	end
end

function test.handHover(isLocal, hovering)
	local self = test -- :(
	if not isLocal then
		self.hands.opponent.hovering = hovering
	end
end

function test.playCard(isLocal, id, target)
	local self = test -- :(
	print((isLocal and "You" or "Opponent") .. " activated card " .. id)

	local card = self:findCard(id)
	local stack = target > 4 and self.golems.opponent[target-4] or self.golems.own[target]
	stack:add(card)

	if not isLocal then
		self.hands.opponent:remove(self.hands.opponent.cards[1])
	end
end

function test.discardCard(isLocal, card)
	local self = test -- :(

	if isLocal then
		self.discards.own:addCard(card)
	else
		local card = self.hands.opponent:removeByPosition(1)
		self.discards.opponent:addCard(card)
	end
end

function test.drawCard(isLocal, card)
	local self = test -- :(

	if isLocal then
		self.hands.own:add(card)
	else
		self.hands.opponent:add(Card{art = "gfx/front.png"})
		self.decks.opponent:drawCard()
	end
end

local function nextGolem(self, i)
	i = (i or 0) + 1
	if i > 8 then
		return nil
	elseif i > 4 then
		return i, self.golems.opponent[i-4], true
	else
		return i, self.golems.own[i], false
	end
end

function test:allGolems()
	return nextGolem, self, nil
end

function test:update(dt)
	self.network:update(dt)

	local x, y = love.mouse.getPosition()
	for i, v, opp in self:allGolems() do
		v:hover(x, y, opp)
	end

	self.hands.own:hover(x, y, false)
end

function test:mousepressed(x, y, button)
	local card = self.hands.own:pick(x, y, false)
	if not card then return end

	if button == 1 then
		signal.emit("PlayCard", true, card.id, 1)
	elseif button == 2 then
		signal.emit("Discard", true, card)
	end
end

function test:keypressed(key)
	if key == "d" then
		local card = self:findCard(self.decks.own:drawCard())
		signal.emit("Draw", true, card)
	end
end

function test:draw()
	love.graphics.setColor(255, 255, 255)

	for i, v, opp in self:allGolems() do
		v:draw(opp)
	end

	-- Battlefield
	self.dummyCard:draw(1184, 360)

	-- Our deck and discard
	self.decks.own:draw(1184, 620, false)
	self.discards.own:draw(96, 620, false)

	-- Opponent's deck and discard
	self.decks.opponent:draw(96, 100, true)
	self.discards.opponent:draw(1184, 100, true)

	self.hands.own:draw(false)
	self.hands.opponent:draw(true)

	for i, v in ipairs(chatlog) do
		love.graphics.print(v, 10, 540+18*i)
	end
end

return test
