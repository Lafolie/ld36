return function(iter, a, b)
	local t = {}
	local key, value = iter(a, b)
	if not key then return t end
	if not value then
		key, value = 1, key
		t[key] = value
		for value in iter, a, value do
			key = key + 1
			t[key] = value
		end
	else
		t[key] = value
		for key, value in iter, a, key do
			t[key] = value
		end
	end
	return t
end
