local enet = require "enet"
local signal = require "lib.hump.signal"
local bitser = require "lib.bitser"
local Destructor = require "Destructor"

local channelMap = {
	Command = { type = "reliable", id = 0, serialised = true },
	Chat = { type = "reliable", id = 1, serialised = false },
	Cosmetic = { type = "unreliable", id = 2, serialised = true },
}
local channelReverseMap = {}
local channelCount = 0

for i, v in pairs(channelMap) do
	channelCount = channelCount + 1
	channelReverseMap[v.id] = v
	v.name = i
end

return class "Network"
{
	__init__ = function(self)
		self.host = nil
		self.peer = nil

		self.signalHandlers = {}
		for i, v in pairs(channelMap) do
			local function handler(msg)
				return self:send(v.name, msg)
			end
			local signame = "send-" .. v.name
			signal.register(signame, handler)
			table.insert(self.signalHandlers, {signame, handler})
		end
	end,

	close = function(self)
		if self.__destructed then return end
		self.__destructed = true
		for i, v in ipairs(self.signalHandlers) do
			signal.remove(unpack(v))
		end
	end,

	startServer = function(self)
		self.host = enet.host_create("*:4004", 1, channelCount)
	end,

	connect = function(self, target)
		self.host = enet.host_create()
		self.host:connect(target .. ":4004", channelCount)
	end,

	update = function(self)
		if not self.host then return end

		while true do
			local event = self.host:service()
			if not event then break end

			if event.type == "connect" then
				self.peer = event.peer
			elseif event.type == "disconnect" then
				self.peer = nil
			elseif event.type == "receive" then
				local channel = channelReverseMap[event.channel]
				if channel.serialised then
					event.data = bitser.loads(event.data)
				end
				signal.emit("recv-" .. channel.name, event.data)
			end
		end
	end,

	isConnected = function(self)
		return self.peer ~= nil
	end,

	send = function(self, channel, data)
		if not self:isConnected() then
			print("No connection, message dropped.")
			return
		end

		channel = assert(channelMap[channel], "Unknown channel")
		if channel.serialised then
			data = bitser.dumps(data)
		end
		self.peer:send(data, channel.id, channel.type)
	end,
}
