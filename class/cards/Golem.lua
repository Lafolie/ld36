local tablecomp = require "lib.tablecomp"
local Card = require "Card"

return class "cards.Golem" (Card)
{
	__init__ = function(self, def)
		Card.__init__(self, def)

		self.maxHealth = def.hp
		self.health = self.maxHealth

		self.slots = {
			Arm = {limit = 2},
			Body = {limit = 1},
			Extra = {limit = 1},
		}
	end,

	attach = function(self, slot, relic)
		if #self.slots[slot] >= self.slots[slot].limit then
			return false
		end

		local thisType = self.def.type:sub(1, 1):lower()
		if not relic.def.golem:match(thisType) then
			return false
		end

		table.insert(self.slots[slot], relic)
		return true
	end,

	detach = function(self, relic)
		for _, slot in pairs(self.slots) do
			for i, v in ipairs(slot) do
				if v == relic then
					table.remove(slot, i)
					return
				end
			end
		end
	end,

	draw = function(self, x, y, opponent)
		Card.draw(self, x, y, opponent)
		if self.flipRotation == 0 then
			local healthText = ("%2d/%d"):format(self.health, self.maxHealth)
			local s, d = 1+self.height, opponent and -1 or 1
			x, y = x - 128/2*s*d + 103*s*d, y - 192/2*s*d + 2*s*d
			love.graphics.print(healthText, x, y, opponent and math.pi or 0, s)
		end
	end,
}
