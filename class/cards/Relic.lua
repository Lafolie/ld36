local Card = require "Card"

return class "cards.Relic" (Card)
{
	__init__ = function(self, def)
		Card.__init__(self, def)

		self.golem = nil
	end,

	attach = function(self, golem)
		self.golem = golem
	end,

	detach = function(self)
		self.golem = nil
	end,
}
