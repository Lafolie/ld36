local Card = require "Card"

return class "cards.Crystal" (Card)
{
	__init__ = function(self, def)
		Card.__init__(self, def)
		self.strength = def.strength or 1
	end,
}
