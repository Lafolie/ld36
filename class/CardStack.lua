local vector = require "lib.hump.vector"

local function posFor(i)
	i = i - 1
	return -16*i, 16*i
end

local function posNext(stack, i)
	i = i - 1
	if i < 1 then return nil end
	return i, posFor(i)
end

return class "CardStack"
{
	__init__ = function(self, x, y)
		self.cards = {}
		self.height = 0
		self.x, self.y = x or 0, y or 0
	end,

	add = function(self, card)
		table.insert(self.cards, card)
		self:simplify()
	end,

	remove = function(self, card)
		local found = false
		for i, v in ipairs(self.cards) do
			if v == card then
				table.remove(self.cards, i)
				found = true
				break
			end
		end

		if found then
			self:simplify()
		end
		return found
	end,

	simplify = function(self)
		local info = {}
		local idOrder = {}
		local needsSorting = false

		-- Gather information
		for i, v in ipairs(self.cards) do
			if not info[v.id] then
				info[v.id] = {start = i, lastCont = i, count = 1, contiguous = true}
				table.insert(idOrder, v.id)
			elseif i == info[v.id].lastCont+1 then
				info[v.id].lastCont = i
				info[v.id].count = info[v.id].count + 1
			else
				info[v.id].count = info[v.id].count + 1
				info[v.id].contiguous = false
				needsSorting = true
			end
		end

		-- Sort
		if needsSorting then
			local newCards = {}
			for i, v in ipairs(idOrder) do
				-- Contiguous in source, fast path
				if info[v].contiguous then
					for j = info[v].start, info[v].lastCont do
						table.insert(newCards, self.cards[j])
					end
				else -- Otherwise find them exhaustively
					for j, w in ipairs(self.cards) do
						if w.id == v then
							table.insert(newCards, w)
						end
					end
				end
			end

			self.cards = newCards
		end

		-- Save summary info
		self.summary = {}
		for i, v in ipairs(idOrder) do
			self.summary[i] = {id = v, count = info[v].count}
		end
	end,

	posPairs = function(self)
		return posNext, self, #self.cards+1
	end,

	flipBack = function(self)
		for i, v in ipairs(self.cards) do
			v:flipBack(true)
		end
	end,

	flipFront = function(self)
		for i, v in ipairs(self.cards) do
			v:flipFront(true)
		end
	end,

	hover = function(self, targetx, targety, opponent)
		targetx, targety = targetx-self.x, targety-self.y
		local w, h, s, d = 128, 192, (1+self.height), opponent and -1 or 1
		w, h = w*s, h*s
		local hovering
		for i, x, y in self:posPairs() do
			x, y = (x*d-w/2)*s, (y*d-h/2)*s
			if targetx >= x and targetx < x+w and targety >= y and targety < y+h then
				-- Usually we take the top-most card
				hovering = i
				-- Unless we were already hovering over this particular
				-- card (so it was "expanded")
				if i == self.hovering then break end
			end
		end
		self.hovering = hovering
	end,

	draw = function(self, opponent)
		local scale = opponent and -1 or 1
		scale = scale * (1+self.height)
		for i, sx, sy in self:posPairs() do
			self.cards[i].height = self.height
			self.cards[i]:draw(self.x+sx*scale, self.y+sy*scale, opponent)
		end
		if self.hovering then
			local sx, sy = posFor(self.hovering)
			self.cards[self.hovering]:draw(self.x+sx*scale, self.y+sy*scale, opponent)
		end
	end,
	
	drawList = function(self, x, y, selected)
	
		for i, v in ipairs(self.cards) do
			if i == selected then
				v.height = 0.1
			else
				v.height = 0
			end
			v:draw(x, y+i*20 + ((selected and i > selected) and 205 or 0) + (i == selected and 15 or 0) )
		end
	end,
}
