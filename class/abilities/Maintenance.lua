local Ability = require "Ability"

return class "abilities.Maintenance" (Ability)
{
	stackWith = function(self, other)
		return self:stackWithMerge(other)
	end,

	startTurn = function(self, stack)
		stack:damage(-self.level)
	end,
}
