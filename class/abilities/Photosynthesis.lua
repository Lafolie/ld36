local Ability = require "Ability"

return class "abilities.Photosynthesis" (Ability)
{
	stackWith = function(self, other)
		return self:stackWithMerge(other)
	end,

	startTurn = function(self, stack)
		stack:addPower(self.level)
	end,
}
