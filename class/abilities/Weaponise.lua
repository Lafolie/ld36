local Ability = require "Ability"

return class "abilities.Weaponise" (Ability)
{
	attach = function(self, stack)
		local slots = stack.golem.slots
		self.prevLimits = {
			Arm = slots.Arm.limit,
			Body = slots.Body.limit,
			Extra = slots.Extra.limit,
		}

		slots.Arm.limit = 3
		slots.Body.limit = 0
		slots.Extra.limit = 0
	end,

	detach = function(self, stacj)
		local slots = stack.golem.slots

		slots.Arm.limit = self.prevLimits.Arm
		slots.Body.limit = self.prevLimits.Body
		slots.Extra.limit = self.prevLimits.Extra
	end,
}
