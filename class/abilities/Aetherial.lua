local Ability = require "Ability"

return class "abilities.Aetherial" (Ability)
{
	attach = function(self, stack)
		return self:countersAttached(stack)
	end,

	countersAttached = function(self, stack)
		for i = #stack.counters, 1, -1 do
			local type = stack.counters[i]
			if type == "Ice" or type == "Fire" then
				table.remove(stack.counters, i)
			end
		end
	end,
}
