local Ability = require "Ability"

return class "abilities.Attack" (Ability)
{
	canActivate = function(self)
		return true
	end,

	needsTarget = function(self)
		return true
	end,

	activate = function(self, stack, target)
		target:damage(self.level)
	end,
}
