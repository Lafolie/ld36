local Ability = require "Ability"

return class "abilities.Needles" (Ability)
{
	stackWith = function(self, other)
		return self:stackWithMerge(other)
	end,

	damaged = function(self, stack, damageType, damage, attacker)
		if attacker then
			attacker:damage(self.level)
		end
	end,
}
