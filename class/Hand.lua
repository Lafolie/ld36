local signal = require "lib.hump.signal"

return class "Hand"
{
	__init__ = function(self)
		self.cards = {}
		self.hovering = nil
	end,

	add = function(self, card)
		table.insert(self.cards, card)
	end,

	remove = function(self, card)
		for i, v in ipairs(self.cards) do
			if v == card then
				table.remove(self.cards, i)
				return true
			end
		end
		return false
	end,

	removeByPosition = function(self, position)
		return table.remove(self.cards, position)
	end,

	flipFront = function(self)
		for i, v in ipairs(self.cards) do
			v:flipFront(true)
		end
	end,

	flipBack = function(self)
		for i, v in ipairs(self.cards) do
			v:flipBack(true)
		end
	end,

	getDrawParameters = function(self, opponent)
		local xStart = 720 - #self.cards*96/2
		local yPos = opponent and -48 or 660
		local xDist = 96
		local hoverYOffset = opponent and 12 or -40

		return xStart-xDist, yPos, xDist, hoverYOffset
	end,

	hover = function(self, targetx, targety, opponent)
		local xStart, yPos, xDist, hoverYOffset = self:getDrawParameters(opponent)

		local hovering
		local w, h = 128, 192
		for i, v in ipairs(self.cards) do
			local x, y = xStart + i*xDist - w/2, yPos - h/2
			if targetx >= x and targetx < x+w and targety >= y and targety < y+h then
				hovering = i
			end
		end

		if hovering ~= self.hovering and not opponent then
			signal.emit("HandHover", true, hovering)
		end

		self.hovering = hovering
	end,

	pick = function(self, targetx, targety, opponent)
		-- Determine which card we're picking
		self:hover(targetx, targety, opponent)
		if not self.hovering then return end

		-- Now remove that card
		local card = table.remove(self.cards, self.hovering)

		-- ... and update our hover state (again)
		self:hover(targetx, targety, opponent)
		return card
	end,

	draw = function(self, opponent)
		local xStart, yPos, xDist, hoverYOffset = self:getDrawParameters(opponent)
		for i, v in ipairs(self.cards) do
			if i ~= self.hovering then
				v.height = 0
				v:draw(xStart + i*xDist, yPos, opponent)
			end
		end
		if self.hovering then
			local i, v = self.hovering, self.cards[self.hovering]
			v.height = 0.1
			v:draw(xStart + i*xDist, yPos + hoverYOffset, opponent)
		end
	end,
}
