local Card = require "Card"
local Golem = require "cards.Golem"

return class "Deck"
{
	__init__ = function(self, cards)
		self.cards = cards or {}

		self.displayCard = Card{art = "gfx/cardback.png"}
		self.displayCard:flipBack(true)
	end,

	shuffle = function(self)
		local rng = love.math.newRandomGenerator(love.math.random(2^50))

		local new = {}
		for i = 1, #self.cards do
			local card = table.remove(self.cards, rng:random(#self.cards))
			new[i] = card
		end

		self.cards = new
	end,

	drawCard = function(self)
		return table.remove(self.cards)
	end,

	addCard = function(self, card)
		table.insert(self.cards, card)
	end,

	removeGolems = function(self)
		local golems = {}
		for i = #self.cards, 1, -1 do
			if class.isinstance(self.cards[i], Golem) then
				table.insert(golems, table.remove(self.cards, i))
			end
		end

		return golems
	end,

	draw = function(self, x, y, opponent)
		if #self.cards > 0 then
			self.displayCard:draw(x, y, opponent)
		end
	end,
}
