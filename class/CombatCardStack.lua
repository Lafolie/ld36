local CardStack = require "CardStack"
local cards = {
	Golem = require "cards.Golem",
	Relic = require "cards.Relic",
	Crystal = require "cards.Crystal",
}

return class "CombatCardStack" (CardStack)
{
	__init__ = function(self, ...)
		CardStack.__init__(self, ...)

		self.abilities = {}
		self.activatableAbilities = {}
		self.crystals = 0
		self.golem = nil
	end,

	add = function(self, card)
		if #self.cards == 0 then
			if not class.isinstance(card, cards.Golem) then
				error("Invalid move")
			end 

			self.golem = card
		else
			if not class.isinstance(card, cards.Relic) and not class.isinstance(card, cards.Crystal) then
				error("Invalid move")
			end

			if class.isinstance(card, cards.Relic) then
				if not self.cards[1]:attach(card.def.slot, card) then
					error("Invalid move")
				end
				card:attach(self.cards[1])
			end
		end

		CardStack.add(self, card)

		self:updateAbilities()
		self:updateCrystals()
	end,

	remove = function(self, card)
		if class.isinstance(card, cards.Golem) then
			error("Invalid move")
		elseif class.isinstance(card, cards.Relic) then
			self.cards[1]:detach(card)
			card:detach()
		end

		if not CardStack.remove(self, card) then
			return false
		end

		self:updateAbilitiesAndKeywords()
		self:updateCrystals()
		return true
	end,

	updateAbilities = function(self)
		local abs = {}

		for _, card in ipairs(self.cards) do
			if card.abilities then
				for _, ability in ipairs(card.abilities) do
					if abs[ability.__name__] then
						abs[ability.__name__] = abs[ability.__name__]:stackWith(ability)
					else
						abs[ability.__name__] = ability
					end
				end
			end
		end

		self.abilities = {}
		self.activatableAbilities = {}

		for abilityName, ability in pairs(abs) do
			table.insert(self.abilities, ability)
			if ability:canActivate() then
				table.insert(self.activatableAbilities, ability)
			end
		end

		table.sort(self.abilities, function(a, b) return a.__name__ < b.__name__ end)
	end,

	updateCrystals = function(self)
		self.crystals = 0

		for _, card in ipairs(self.cards) do
			if class.isinstance(card, cards.Crystal) then
				self.crystals = self.crystals + card.strength
			end
		end
	end,

	damage = function(self, amount)
		self.golem.health = math.max(0, math.min(self.golem.maxHealth, self.golem.health - amount))
		-- TODO: Ability triggers?
	end,
}
