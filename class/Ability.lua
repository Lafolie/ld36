return class "Ability"
{
	__init__ = function(self, level)
		self.level = level or 1
	end,

	serialize = function(self)
		local name = self.__name__:match("[^.]+$"):lower()
		return ("%s %d"):format(name, self.level)
	end,

	-- To allow (or disallow) stacking
	stackWith = function(self, other)
		return self:stackWithHighest(other)
	end,

	-- Helpers for specific abilities
	stackWithHighest = function(self, other)
		if other.level > self.level then
			return other
		else
			return self
		end
	end,

	stackWithMerge = function(self, other)
		return self.__class__(self.level+other.level)
	end,

	-- For "every turn" abilities
	startTurn = function(self, stack)
	end,

	-- For start boosts
	attach = function(self, stack)
	end,

	detach = function(self, stack)
	end,

	-- For shields
	damaged = function(self, stack, damageType, damage, attacker)
		return damage
	end,

	countersAttached = function(self, stack)
	end,

	-- For "activated" abilities
	canActivate = function(self)
		return false
	end,

	needsTarget = function(self)
		return false
	end,

	activate = function(self, stack, target)
	end,
}
