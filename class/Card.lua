local cache = require "lib.cache"
local requireall = require "lib.requireall"

local HALFPI=math.pi/2

local function sign(x)
	return x > 0 and 1 or -1
end

return class "Card"
{
	__init__ = function(self, def)
		self.def = def
		self.front = self.renderCardFront(def)
		self.back = cache.image("gfx/cardback.png")
		self.id = def.id or tostring(love.math.random())
		self.flipRotation = 0
		self.flipDirection = 0
		self.height = 0
		self.abilities = def.abilities and self.parseAbilities(def.abilities) or {}

		self.w, self.h = self.front:getDimensions()
		local w, h = self.w, self.h
		self.mesh = love.graphics.newMesh{
			{w/2, h/2, 0.5, 0.5},
			{0, 0, 0, 0},
			{w, 0, 1, 0},
			{w, h, 1, 1},
			{0, h, 0, 1},
		}
		self.mesh:setVertexMap(1, 2, 3, 4, 5, 2)
		self.mesh:setTexture(self.front)
	end,
	
	clone = function(self)
		return self.__class__(self.def)
	end,

	renderCardFront = function(def)
		local frame = cache.image("gfx/frame.png")
		local art = cache.image(def.art)

		local front = love.graphics.newCanvas(128, 192)

		local prevCanvas = love.graphics.getCanvas()
		love.graphics.setCanvas(front)
		love.graphics.clear(255, 255, 255, 0)

		love.graphics.setColor(255, 255, 255)
		love.graphics.draw(art)
		love.graphics.draw(frame)

		love.graphics.printf(def.name or "", 7, 3, 96, "left")
		love.graphics.printf(def.description or "", 15, 113, 96, "left")

		love.graphics.setCanvas(prevCanvas)

		return front
	end,

	parseAbilities = function(abilitiesText)
		local abilityImplementations = {}
		do
			local cap = requireall "class.abilities"
			for i, v in pairs(cap) do
				abilityImplementations[i:lower()] = v
			end
		end

		local abilities = {}
		for ability, param in abilitiesText:gmatch("(%w+)%s?(%d*)") do
			if param == "" then
				param = 1
			else
				param = tonumber(param)
			end

			if abilityImplementations[ability] then
				table.insert(abilities, abilityImplementations[ability](param))
			else
				print("Unknown ability: " .. ability)
			end
		end

		return abilities
	end,

	update = function(self, dt)
		self:updateFlip(dt)
	end,

	updateFlip = function(self, dt)
		if self.flipDirection == 0 then return end

		local prevRotation = self.flipRotation
		self.flipRotation = math.max(math.min(self.flipRotation + self.flipDirection * HALFPI * dt, math.pi), 0)
		if self.flipRotation == math.pi or self.flipRotation == 0 then
			self.flipDirection = 0
		end

		if sign(self.flipRotation-HALFPI) ~= sign(prevRotation-HALFPI) then
			self.mesh:setTexture(self.flipRotation <= HALFPI and self.front or self.back)
		end

		local xoff = self.w/2 * (1-math.cos(self.flipRotation))
		local yoff = self.h*0.05*math.sin(self.flipRotation)

		self.mesh:setVertex(2, xoff, -yoff, 0, 0)
		self.mesh:setVertex(3, self.w-xoff, yoff, 1, 0)
		self.mesh:setVertex(4, self.w-xoff, self.h-yoff, 1, 1)
		self.mesh:setVertex(5, xoff, self.h+yoff, 0, 1)
	end,

	flipFront = function(self, immediate)
		self.flipDirection = -1
		if immediate then
			self.flipDirection = -math.huge
			self:updateFlip(1)
		end
	end,

	flipBack = function(self, immediate)
		self.flipDirection = 1
		if immediate then
			self.flipDirection = math.huge
			self:updateFlip(1)
		end
	end,

	draw = function(self, x, y, opponent)
		local s = 1+self.height
		local r = opponent and math.pi or 0
		love.graphics.draw(self.mesh, x, y, r, s, s, self.w/2, self.h/2)
	end,
}
