local tablecomp = require "lib.tablecomp"

local cache = {}

return class "Database"
{
	readTsv = function(cls, filename)
		local data = {}
		for line in love.filesystem.lines(filename) do
			local fields = tablecomp(line:gmatch("([^\t]*)\t?"))
			table.insert(data, fields)
		end

		local header = table.remove(data, 1)
		for i = 1, #data do
			local row = data[i]
			for fieldNo, fieldName in ipairs(header) do
				local value = row[fieldNo]
				if tonumber(value) then value = tonumber(value) end
				if value == "" then value = nil end
				row[fieldName] = value
				row[fieldNo] = nil
			end
			data[row.id] = row
			data[i] = nil
		end

		return data
	end,

	getTable = function(cls, name)
		if not cache[name] then
			cache[name] = cls:readTsv("data/" .. name .. ".tsv")
		end
		return cache[name]
	end,
}
